const $ = require('jquery');
const electron = require('electron');
const remote = electron.remote;
const Menu = remote.Menu;
const mainProcess = remote.require('./index');

var constants = require('../../components/constants.js');
var SmashladderApi = require("../../components/SmashladderApi.js");
var Authentication = require('../../components/Authentication');
var JsonHelper = require('../../components/JsonHelper');

let loginAuthCode = $('#login_2');
let tokenInput = $('#ladder_code');

loginAuthCode.find('.retrieve_code').text('Click here to retrieve a login code')
    .attr('href', constants.SITE_URL+'/api/v1/dolphin/credentials_link');

loginAuthCode.on('setError', function(e, message){
    let tokenInput = loginAuthCode.find('#ladder_code');
    let errorMessage = loginAuthCode.find('.error-message');
    if(message === false)
    {
        tokenInput.removeClass('invalid');
        tokenInput.removeClass('valid');
    }
    else if(message === true)
    {
        tokenInput.removeClass('invalid');
        tokenInput.addClass('valid');
    }
    else
    {
        tokenInput.addClass('invalid');
        errorMessage.text(message);
    }
});
loginAuthCode.on('input', function(e){
    let token = loginAuthCode.find('#ladder_code').val();
    loginAuthCode.trigger('setError', false);
    loginAuthCode.trigger('submit');
});
loginAuthCode.on('submit', function(e){
    e.preventDefault();
    if(loginAuthCode.hasClass('loading'))
    {
        console.log('has loading, ignore');
        return;
    }
    let inputs = loginAuthCode.find(':input');
    loginAuthCode.addClass('loading');
    inputs.prop('disabled', true);


    var loading = loginAuthCode.find('.preloader-wrapper').addClass('active');
    let tokenInput = loginAuthCode.find('#ladder_code');

    if(tokenInput.val().length == 0)
    {
        loginAuthCode.trigger('setError', false);
        finished();
        return;
    }

    function finished()
    {
        loginAuthCode.removeClass('loading');
        loading.removeClass('active');
        inputs.prop('disabled', false);
    }
    let token = tokenInput.val();
    var authentication = new Authentication(token);
    authentication.save().then(()=>{
        return authentication.checkAuthentication()
    }).then(function(authentication){
        console.log(authentication);
        console.log('[LOGIN RESPONSE]', authentication);
        return authentication;
    }).then((authentication)=>{
        mainProcess.loginReady(authentication.player);
        tokenInput.val('');
        finished();
    }).catch(function(error){
        console.log('setting error?');
        let message = error.error || error.message;
        if(error.statusCode)
        {
            message = null;
            if(error.statusCode == 401)
            {
                tokenInput.val('');
                message = 'This login is no longer valid';

            }
            else
            {
                message = 'There was a server error, please try again later?';
            }
        }
        if(message)
        {
            loginAuthCode.trigger('setError', [message]);
        }
        else
        {
            loginAuthCode.trigger('setError', ['Connection Issue']);
        }
        finished();
    });

});


const InputMenu = Menu.buildFromTemplate(
    [
    {
        label: 'Cut',
        role: 'cut',
    }, {
        label: 'Copy',
        role: 'copy',
    }, {
        label: 'Paste',
        role: 'paste',
    }, {
        type: 'separator',
    }, {
        label: 'Select all',
        role: 'selectall',
    }
]);

document.body.addEventListener('contextmenu', (e) => {
    e.preventDefault();
    e.stopPropagation();

    let node = e.target;

    while (node) {
        if (node.nodeName.match(/^(input|textarea)$/i) || node.isContentEditable) {
            InputMenu.popup(remote.getCurrentWindow());
            break;
        }
        node = node.parentNode;
    }
});