
require('./MeleeAction');
class MatchPlayerActions
{
    constructor(matchPlayer){
        this.player = matchPlayer;
        this.lastAction = null;

        this.actions = 0;
        this.startSeconds = this.player.match.timeRemaining;
        this.endSeconds = this.player.match.timeRemaining;
    }

    setAction(action){
        if(action.isApmAction)
        {
            if(action === this.lastAction || action.isSameGroupAs(this.lastAction))
            {

            }
            else
            {
                this.actions++;
            }
        }
        else
        {

        }
        this.lastAction = action;
        this.endSeconds = this.player.match.timeRemaining;
    }

    getActionsPerMinute(){
        let secondsElapsed = this.startSeconds - this.endSeconds;
        if(secondsElapsed <= 3)
        {
            return null;
        }
        let actionsPerSecond = this.actions / secondsElapsed;
        let actionsPerMinute = Math.round(actionsPerSecond * 60);
        return actionsPerMinute;
    }

    getTotalActions(){
        return this.actions;
    }
}
module.exports = MatchPlayerActions;
