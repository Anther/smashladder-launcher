class MeleeAction {
    constructor(name, humanName, index, hexValue){
        this.index = index;
        this.humanName = humanName;
        this.name = name;
        this.hexValue = hexValue;

        this.isApmAction = false;
        this.similarActionGroup = null;
    }

    readableName(){
        if(this.humanName)
        {
            return this.humanName;
        }
        if(this.name)
        {
            return this.name;
        }
        return this.hexValue;
    }

    isSameGroupAs(action){
        if(!this.similarActionGroup)
        {
            return false;
        }
        if(!action)
        {
            return false;
        }
        return this.similarActionGroup.actions[action.index] ? true : false;
    }

    toString(){
        return ''+this.name + '('+this.humanName+ ')' + this.index + ' ' + this.hexValue;
    }

    static retrieve(action){
        if(byIndex[action]){
            return byIndex[action];
        }
        else{
            if(Number.isInteger(action)){
                return new MeleeAction('UNKNOWN', -1, action);
            }
            else{
                return new MeleeAction('UNKNOWN-BLANK', -1, action);
            }
        }
    }

    static setApmable(index, similarActionGroup){
        byIndex[index].isApmAction = true;
        if(similarActionGroup)
        {
            byIndex[index].similarActionGroup = similarActionGroup;
            similarActionGroup.add(byIndex[index]);
        }
        return byIndex[index];
    }
}

class MeleeActionGroup
{
    constructor(){
        this.actions = {};
    }
    add(action){
        this.actions[action.index] = action;
    }
}

module.exports = MeleeAction;

let actionCodes = ["0000","0001","0002","0003","0004","0005","0006","0007","0008","0009","000A","000B","000C","000D","000E","000F","0010","0011","0012","0013","0014","0015","0016","0017","0018","0019","001A","001B","001C","001D","001E","001F","0020","0021","0022","0023","0024","0025","0026","0027","0028","0029","002A","002B","002C","002D","002E","002F","0030","0031","0032","0033","0034","0035","0036","0037","0038","0039","003A","003B","003C","003D","003E","003F","0040","0041","0042","0043","0044","0045","0046","0047","0048","0049","004A","004B","004C","004D","004E","004F","0050","0051","0052","0053","0054","0055","0056","0057","0058","0059","005A","005B","005C","005D","005E","005F","0060","0061","0062","0063","0064","0065","0066","0067","0068","0069","006A","006B","006C","006D","006E","006F","0070","0071","0072","0073","0074","0075","0076","0077","0078","0079","007A","007B","007C","007D","007E","007F","0080","0081","0082","0083","0084","0085","0086","0087","0088","0089","008A","008B","008C","008D","008E","008F","0090","0091","0092","0093","0094","0095","0096","0097","0098","0099","009A","009B","009C","009D","009E","009F","00A0","00A1","00A2","00A3","00A4","00A5","00A6","00A7","00A8","00A9","00AA","00AB","00AC","00AD","00AE","00AF","00B0","00B1","00B2","00B3","00B4","00B5","00B6","00B7","00B8","00B9","00BA","00BB","00BC","00BD","00BE","00BF","00C0","00C1","00C2","00C3","00C4","00C5","00C6","00C7","00C8","00C9","00CA","00CB","00CC","00CD","00CE","00CF","00D0","00D1","00D2","00D3","00D4","00D5","00D6","00D7","00D8","00D9","00DA","00DB","00DC","00DD","00DE","00DF","00E0","00E1","00E2","00E3","00E4","00E5","00E6","00E7","00E8","00E9","00EA","00EB","00EC","00ED","00EE","00EF","00F0","00F1","00F2","00F3","00F4","00F5","00F6","00F7","00F8","00F9","00FA","00FB","00FC","00FD","00FE","00FF","0100","0101","0102","0103","0104","0105","0106","0107","0108","0109","010A","010B","010C","010D","010E","010F","0110","0111","0112","0113","0114","0115","0116","0117","0118","0119","011A","011B","011C","011D","011E","011F","0120","0121","0122","0123","0124","0125","0126","0127","0128","0129","012A","012B","012C","012D","012E","012F","0130","0131","0132","0133","0134","0135","0136","0137","0138","0139","013A","013B","013C","013D","013E","013F","0140","0141","0142","0143","0144","0145","0146","0147","0148","0149","014A","014B","014C","014D","014E","014F","0150","0151","0152","0153","0154","0155","0156","0157","0158","0159","015A","015B","015C","015D","015E","015F","0160","0161","0162","0163","0164","0165","0166","0167","0168","0169","016A","016B","016C","016D","016E","016F","0170","0171","0172","0173","0174","0175","0176","0177","0178","0179","017A","017B","017C","017D","017E"];
let actionCommands = ["DeadDown","DeadLeft","DeadRight","DeadUp","DeadUpStar","DeadUpStarIce","DeadUpFall","DeadUpFallHitCamera","DeadUpFallHitCameraFlat","DeadUpFallIce","DeadUpFallHitCameraIce","Sleep","Rebirth","RebirthWait","Wait","WalkSlow","WalkMiddle","WalkFast","Turn","TurnRun","Dash","Run","RunDirect","RunBrake","KneeBend","JumpF","JumpB","JumpAerialF","","JumpAerialB","FallF","FallB","FallAerial","FallAerialF","FallAerialB","FallSpecial","FallSpecialF","FallSpecialB","DamageFall","Squat","SquatWait","SquatRv","Landing","LandingFallSpecial","Attack11","Attack12","Attack13","Attack100Start","Attack100Loop","Attack100End","AttackDash","AttackS3Hi","AttackS3HiS","AttackS3S","AttackS3LwS","AttackS3Lw","AttackHi3","AttackLw3","AttackS4Hi","AttackS4HiS","AttackS4S","AttackS4LwS","AttackS4Lw","AttackHi4","AttackLw4","AttackAirN","AttackAirF","AttackAirB","AttackAirHi","AttackAirLw","LandingAirN","LandingAirF","LandingAirB","LandingAirHi","LandingAirLw","DamageHi1","DamageHi2","DamageHi3","DamageN1","DamageN2","DamageN3","DamageLw1","DamageLw2","DamageLw3","DamageAir1","DamageAir2","DamageAir3","DamageFlyHi","DamageFlyN","DamageFlyLw","DamageFlyTop","DamageFlyRoll","LightGet","HeavyGet","LightThrowF","LightThrowB","LightThrowHi","LightThrowLw","LightThrowDash","LightThrowDrop","LightThrowAirF","LightThrowAirB","LightThrowAirHi","LightThrowAirLw","HeavyThrowF","HeavyThrowB","HeavyThrowHi","HeavyThrowLw","LightThrowF4","LightThrowB4","LightThrowHi4","LightThrowLw4","LightThrowAirF4","LightThrowAirB4","LightThrowAirHi4","LightThrowAirLw4","HeavyThrowF4","HeavyThrowB4","HeavyThrowHi4","HeavyThrowLw4","SwordSwing1","SwordSwing3","SwordSwing4","SwordSwingDash","BatSwing1","BatSwing3","BatSwing4","BatSwingDash","ParasolSwing1","ParasolSwing3","ParasolSwing4","ParasolSwingDash","HarisenSwing1","HarisenSwing3","HarisenSwing4","HarisenSwingDash","StarRodSwing1","StarRodSwing3","StarRodSwing4","StarRodSwingDash","LipStickSwing1","LipStickSwing3","LipStickSwing4","LipStickSwingDash","ItemParasolOpen","ItemParasolFall","ItemParasolFallSpecial","ItemParasolDamageFall","LGunShoot","LGunShootAir","LGunShootEmpty","LGunShootAirEmpty","FireFlowerShoot","FireFlowerShootAir","ItemScrew","ItemScrewAir","DamageScrew"," DamageScrewAir","ItemScopeStart","ItemScopeRapid","ItemScopeFire","ItemScopeEnd","ItemScopeAirStart","ItemScopeAirRapid","ItemScopeAirFire","ItemScopeAirEnd","ItemScopeStartEmpty","ItemScopeRapidEmpty","ItemScopeFireEmpty","ItemScopeEndEmpty","ItemScopeAirStartEmpty","ItemScopeAirRapidEmpty","ItemScopeAirFireEmpty","ItemScopeAirEndEmpty","LiftWait","LiftWalk1","LiftWalk2","LiftTurn","GuardOn","Guard","GuardOff","GuardSetOff","GuardReflect","DownBoundU","DownWaitU","DownDamageU","DownStandU","DownAttackU","DownFowardU","DownBackU","DownSpotU","DownBoundD","DownWaitD","DownDamageD","DownStandD","DownAttackD","DownFowardD","DownBackD","DownSpotD","Passive","PassiveStandF","PassiveStandB","PassiveWall","PassiveWallJump","PassiveCeil","ShieldBreakFly","ShieldBreakFall","ShieldBreakDownU","ShieldBreakDownD","ShieldBreakStandU","ShieldBreakStandD","FuraFura","Catch","CatchPull","CatchDash","CatchDashPull","CatchWait","CatchAttack","CatchCut","ThrowF","ThrowB","ThrowHi","ThrowLw","CapturePulledHi","CaptureWaitHi","CaptureDamageHi","CapturePulledLw","CaptureWaitLw","CaptureDamageLw","CaptureCut","CaptureJump","CaptureNeck","CaptureFoot","EscapeF","EscapeB","Escape","EscapeAir","ReboundStop","Rebound","ThrownF","ThrownB","ThrownHi","ThrownLw","ThrownLwWomen","Pass","Ottotto","OttottoWait","FlyReflectWall","FlyReflectCeil","StopWall","StopCeil","MissFoot","CliffCatch","CliffWait","CliffClimbSlow","CliffClimbQuick","CliffAttackSlow","CliffAttackQuick","CliffEscapeSlow","CliffEscapeQuick","CliffJumpSlow1","CliffJumpSlow2","CliffJumpQuick1","CliffJumpQuick2","AppealR","AppealL","ShoulderedWait","ShoulderedWalkSlow","ShoulderedWalkMiddle","ShoulderedWalkFast","ShoulderedTurn","ThrownFF","ThrownFB","ThrownFHi","ThrownFLw","CaptureCaptain","CaptureYoshi","YoshiEgg","CaptureKoopa","CaptureDamageKoopa","CaptureWaitKoopa","ThrownKoopaF","ThrownKoopaB","CaptureKoopaAir","CaptureDamageKoopaAir","CaptureWaitKoopaAir","ThrownKoopaAirF","ThrownKoopaAirB","CaptureKirby","CaptureWaitKirby","ThrownKirbyStar","ThrownCopyStar","ThrownKirby","BarrelWait","Bury","BuryWait","BuryJump","DamageSong","DamageSongWait","DamageSongRv","DamageBind","CaptureMewtwo","CaptureMewtwoAir","ThrownMewtwo","ThrownMewtwoAir","WarpStarJump","WarpStarFall","HammerWait","HammerWalk","HammerTurn","HammerKneeBend","HammerFall","HammerJump","HammerLanding","KinokoGiantStart","KinokoGiantStartAir","KinokoGiantEnd","KinokoGiantEndAir","KinokoSmallStart","KinokoSmallStartAir","KinokoSmallEnd","KinokoSmallEndAir","Entry","EntryStart","EntryEnd","DamageIce","DamageIceJump","CaptureMasterhand","CapturedamageMasterhand","CapturewaitMasterhand","ThrownMasterhand","CaptureKirbyYoshi","KirbyYoshiEgg","CaptureLeadead","CaptureLikelike","DownReflect","CaptureCrazyhand","CapturedamageCrazyhand","CapturewaitCrazyhand","ThrownCrazyhand","BarrelCannonWait","Wait1","Wait2","Wait3","Wait4","WaitItem","SquatWait1","SquatWait2","SquatWaitItem","GuardDamage","EscapeN","AttackS4Hold","HeavyWalk1","HeavyWalk2","ItemHammerWait","ItemHammerMove","ItemBlind","DamageElec","FuraSleepStart","FuraSleepLoop","FuraSleepEnd","WallDamage","CliffWait1","CliffWait2","SlipDown","Slip","SlipTurn","SlipDash","SlipWait","SlipStand","SlipAttack","SlipEscapeF","SlipEscapeB","AppealS","Zitabata","CaptureKoopaHit","ThrownKoopaEndF","ThrownKoopaEndB","CaptureKoopaAirHit","ThrownKoopaAirEndF","ThrownKoopaAirEndB","ThrownKirbyDrinkSShot","ThrownKirbySpitSShot"];
let humanNames = ["Standard downward death","Standard leftward death","Standard rightward death","Upward death used in 1P \"Team Kirby\", etc.","Standard Star KO","Star KO while encased in ice (Freezie)","64-esque front fall, unused, I believe","","","","","\"Nothing\" state, probably - it is the state Sheik/Zelda is in when their counterpart is the one currently playing","Entering on halo","Waiting on halo","Standing state","","","","","","","","","","Pre-jump animation","First jump forward","First jump backward","Aerial jump forward","Aerial jump backward","Falling straight down","Falling with forward DI","Falling with backward DI","Falling after the second jump","ojected","Falling after the second jump with backward DI","Special fall after UpB or airdodge","Special fall with forward DI","Special fall with backward DI","Tumbling","Going from stand to crouch","Crouching","Going from crouch to stand","Landing state, can be cancelled","Landing from special fall","Standard attack 1","Standard attack 2","Standard attack 3","Start of a looping standard attack","Middle of a looping standard attack","End of a looping standard attack","Dash attack","High Ftilt","High-mid Ftilt","Mid Ftilt","Low-mid Ftilt","Low Ftilt","Uptilt","Downtilt","High Fsmash","High-mid Fsmash","Mid Fsmash","Low-mid Fsmash","Low Fsmash","Upsmash","Downsmash","Nair","Fair","Bair","Uair","Dair","Landing during Nair","Landing during Fair","Landing during Bair","Landing during Uair","Landing during Dair","","","","","","","","","","","","","","","","","","Picking up an item","Picking up a heavy item (barrel)","Throwing items at standard speed","","","","","","","","","","","","","","Throwing items at Smash speed","","","","","","","","","","","","Beam sword swings","","","","Home Run Bat swings","","","","Parasol swings","","","","Fan swings","","","","Star Rod swings","","","","Lip's Stick swings","","","","","","","","Raygun shots","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","Holding shield","","Shield stun","","The \"failed to tech\" bounce, facing up","Laying on ground facing up","Getting hit laying on ground facing up","","Get up attack from ground face up","","","","The \"failed to tech\" bounce, facing down","","Lying on the ground","Neutral getup","","","","","Neutral tech","Forward tech","Backward tech","Wall tech","Walljump tech/plain walljump","Ceiling tech","","","","","","","Shield-break tottering","Grab","Successfully grabbing a character - pulling them in","","","Grabbing and holding a character","Pummel","When opponent breaks of a character's grab","","","","","","","","Becoming grabbed","When grabbed","Pummeled","Grab release","","","","roll forward","roll backward","spotdodge","airdodge","","","","","","","","Drop through platform","Ledge teeter","","","","","","","Catching the ledge","Hanging on the ledge","Climbing the ledge, >100%","Climbing the ledge, <100%","Ledge attack, >100%","Ledge attack, <100%","Ledge roll, >100%","Ledge roll, <100%","","","","","Taunt right","Taunt left","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","Super/Poison mushroom states","","","","","","","","Warp in at beginning of match.","","","","","","","","","","","","","","","","","","","Nothing beyond this point is"];

let byIndex = {};
let byName = {};
let byHex = {};
actionCodes.forEach((value,index)=>{
    let hexValue = '0x'+value;
    let indexValue = parseInt(hexValue);
    let humanName = humanNames[indexValue];
    let internalName = actionCommands[indexValue];

    let object = new MeleeAction(
        internalName,
        (humanName && humanName.length)?humanName:null,
        indexValue,
        hexValue
    );
    byIndex[indexValue] = object;
    byName[object.name] = object;
    byHex[object.hexValue] = object;
});

let walkGroup = new MeleeActionGroup();
MeleeAction.setApmable(0x000E); //Complete Stop
MeleeAction.setApmable(0x000F, walkGroup); //walk
MeleeAction.setApmable(0x0010, walkGroup); // walk
MeleeAction.setApmable(0x0011, walkGroup); // walk
MeleeAction.setApmable(0x0014, walkGroup); // dash
MeleeAction.setApmable(0x0015, walkGroup); //run
MeleeAction.setApmable(0x0017); //runbrake

MeleeAction.setApmable(0x0019); // 'Jumping',
MeleeAction.setApmable(0x001A); //'Jumping',
MeleeAction.setApmable(0x001B); //'Jumping',
MeleeAction.setApmable(0x001C); // 'Jumping',
MeleeAction.setApmable(0x0028); // 'Crouch',
MeleeAction.setApmable(0x002C); // 'Standard Attack 1',
MeleeAction.setApmable(0x002D); // 'Standard Attack 2',
MeleeAction.setApmable(0x002E); // 'Standard Attack 3',
MeleeAction.setApmable(0x002F); // 'Looping Attack',
MeleeAction.setApmable(0x0032); // 'DashAttack',
MeleeAction.setApmable(0x0033); // 'DashAttack',
MeleeAction.setApmable(0x0034); // 'DashAttack',
MeleeAction.setApmable(0x0035); // 'DashAttack',
MeleeAction.setApmable(0x0036); // 'DashAttack',
MeleeAction.setApmable(0x0037); // 'DashAttack',
MeleeAction.setApmable(0x0038); // 'DashAttack',
MeleeAction.setApmable(0x0039); // 'DashAttack',
MeleeAction.setApmable(0x003A); // 'DashAttack',
MeleeAction.setApmable(0x003B); // 'DashAttack',
MeleeAction.setApmable(0x003C); // 'DashAttack',
MeleeAction.setApmable(0x003D); // 'DashAttack',
MeleeAction.setApmable(0x003E); // 'DashAttack',
MeleeAction.setApmable(0x003F); // 'DashAttack',
MeleeAction.setApmable(0x0040); // 'DashAttack',
MeleeAction.setApmable(0x0041); // 'DashAttack',
MeleeAction.setApmable(0x0042); // 'DashAttack',
MeleeAction.setApmable(0x0043); // 'DashAttack',
MeleeAction.setApmable(0x0044); // 'DashAttack',
MeleeAction.setApmable(0x0045); // 'DashAttack',
MeleeAction.setApmable(0x00E9); // 'Roll FOrward',
MeleeAction.setApmable(0x00EA); // 'Roll Backwards',
MeleeAction.setApmable(0x00EB); // 'Spotdodge',
MeleeAction.setApmable(0x00EC); // 'Airdodge',
MeleeAction.setApmable(0x00B3); // 'Shield',
MeleeAction.setApmable(0x00C7); // 'Tech',
MeleeAction.setApmable(0x00C8); // 'Tech',
MeleeAction.setApmable(0x00C9); // 'Tech',
MeleeAction.setApmable(0x00CA); // 'Tech',
MeleeAction.setApmable(0x00CB); // 'Tech',
MeleeAction.setApmable(0x00BB); // 'Get up attack',
MeleeAction.setApmable(0x00BC); // 'Get up attack',
MeleeAction.setApmable(0x00BD); // 'Get up attack',
MeleeAction.setApmable(0x00BE); // 'Get up attack',
MeleeAction.setApmable(0x00CC); // 'Ceiling Tech',
MeleeAction.setApmable(0x00C2); // 'Neutral getup',
MeleeAction.setApmable(0x00D4); // 'Grab',
MeleeAction.setApmable(0x00D9); // 'Pummel',
MeleeAction.setApmable(0x00DB); // 'Throw',
MeleeAction.setApmable(0x00DC); // 'Throw',
MeleeAction.setApmable(0x00DD); // 'Throw',
MeleeAction.setApmable(0x00DE); // 'Throw',
MeleeAction.setApmable(0x0159); // 'WaitItem', //Used for b moves...
MeleeAction.setApmable(0x00F4); // 'Drop Through Platform',
MeleeAction.setApmable(0x0166); // 'Fura sleep start???', //Shine??
MeleeAction.setApmable(0x0168); // 'Fura sleep end???', //Shine??
MeleeAction.setApmable(0x015E); // 'EscapeN', //Fox side b??
MeleeAction.setApmable(0x0162); // 'Item Hammer Wait', //Fox up b??
MeleeAction.setApmable(0x0164); // 'ItemBlind', //Pika up b??
MeleeAction.setApmable(0x0108); // 'Taunt', //Pika up b??
MeleeAction.setApmable(0x0109); // 'Taunt', //Pika up b??
MeleeAction.setApmable(0x00FC); // 'Catch ledge', //Pika up b??