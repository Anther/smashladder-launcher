const StringManipulator = require('../StringManipulator');

class MeleeStage
{
    static retrieve(id){
        var name = stages[id];
        if(name)
        {
            if(cache[name])
            {
                return cache[name];
            }
            return cache[name] = new MeleeStage(id, name);
        }
        throw new Error('Stage not found'+ id);
        return null;
    }

    constructor(id, name)
    {
        this.id = id;
        this.name = name;
        this.url = './images/stages/melee/'+StringManipulator.slugify(this.name.toLowerCase())+'.png';
        this.img = '<img class="stage_icon" src="'+this.url+'">';
        // Object.freeze(this);
    }
}

var cache = {};
var stages = {
    2: "Princess Peach's Castle",
    3: "Rainbow Cruise",
    4: "Kongo Jungle",
    5: "Jungle Japes",
    6: "Great Bay",
    7: "Temple",
    8: "Brinstar",
    9: "Brinstar Depths",
    10: "Yoshi's Story",
    11: "Yoshi's Island",
    12: "Fountain of Dreams",
    13: "Green Greens",
    14: "Corneria",
    15: "Venom",
    16: "Pokemon Stadium",
    17: "Poke Floats",
    18: "Mute City",
    19: "Big Blue",
    20: "Onett",
    21: "Fourside",
    22: "Icicle Mountain",
    24: "Mushroom Kingdom",
    25: "Mushroom Kingdom II",
    27: "Flat Zone",
    28: "Dream Land",
    29: "Yoshi's Island (64)",
    30: "Kongo Jungle (64)",
    36: "Battlefield",
    37: "Final Destination",
};

module.exports = MeleeStage;