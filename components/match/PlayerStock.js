const StringManipulator = require('../StringManipulator')
const MatchPlayerActions = require('./MatchPlayerActions');

class PlayerStock
{
    constructor(matchPlayer, stockNumber, imageElement)
    {
        this.matchPlayer = matchPlayer;
        this.stockNumber = stockNumber;
        this.startTime = null;
        this.endTime = null;
        this.container = $('<div>').addClass('stock_container');
        this.imageContainer = $('<div>').addClass('image_container');
        this.percentContainer = $('<span>').addClass('percent_container');
        this.percentContainer.append($('<span>').addClass('number').text(0));
        this.percentContainer.append($('<span>').addClass('percent').text('%'));
        this.image = $(imageElement);
        this.image.appendTo(this.imageContainer);
        this.container.append(this.imageContainer).append(this.percentContainer);
        this.percentContainer = this.percentContainer.find('.number');
        this.element = this.container;
        this.damage = 0;
        this.actions = new MatchPlayerActions(matchPlayer);
    }

    serializeStock(){
        let data = {};
        data.number = this.stockNumber;
        data.percent = this.damage;
        data.started = this.started;
        data.ended = this.ended;
        data.apm = this.actions.getActionsPerMinute();
        data.actions = this.actions.getTotalActions();
        return data;
    }

    startStock(){
        this.started = this.matchPlayer.match.timeRemaining;
        this.element.addClass('started');
        this.damageAtBeginningOfStock =
            this.matchPlayer.stats.damageReceived ? this.matchPlayer.stats.damageReceived : 0;
        this.updateTooltip();
        setTimeout(()=>{
            this.matchPlayer.match.reportForFun();
        },500);
        return this;
    }

    updateTooltip(){
        if(this.ended)
        {
            let sentence = 'Ended at '
                + StringManipulator.timeFormat(this.ended) +' with '+this.damage+'%';
            return this.image.attr('data-tooltip', sentence).tooltip();
        }
        else
        {
            return this.image.tooltip('remove');
        }
    }

    endStock(){
        this.ended = this.matchPlayer.match.timeRemaining;
        this.image.addClass('dead');
        this.updateTooltip();
        return this;
    }

    updateDamage(damage){
        this.damage = damage - this.damageAtBeginningOfStock;
        this.percentContainer.cacheText(this.damage);
        return this;
    }
}

module.exports = PlayerStock;