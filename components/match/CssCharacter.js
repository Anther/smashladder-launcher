var MeleeCharacter = require("./MeleeCharacter.js");
class CssCharacter
{
    static retrieve(slot){
        if(CssCharacter.characters[slot])
        {
            return CssCharacter.characters[slot];
        }
        else
        {
            return CssCharacter.characters[slot] = new CssCharacter(slot);
        }
    }

    constructor(slot){
        this.slot = slot;
        this.element = CssCharacter.getTemplateElement();
        this.element.appendTo(CssCharacter.templateContainer);
        this.element.findCache('.player_number').text(slot);
    }

    updatePlayerData(playerData){
        this.playerData = playerData;
        var newCharacter;
        var newHover;

        if(CssCharacter.visiblePlayerTypes[this.playerData.playerType])
        {
            try{
                newCharacter = MeleeCharacter.retrieveCss(playerData.cssCharacter);
            }
            catch(e){
                newCharacter = null;
            }
        }
        else
        {
            newCharacter = null;
        }

        if(!newCharacter)
        {
            this.character = null;
            this.updateElement();
            return;
        }
        if(this.character != newCharacter || this.costume != playerData.costume )
        {
            this.character = MeleeCharacter.retrieveCss(playerData.cssCharacter);
            this.costume = playerData.costume;
            this.updateElement();
        }
    }

    updateElement(){
        if(this.character)
        {
            this.element.findCache('.character_icon').html(this.character.stockIcon(this.playerData.costume))
        }
        else
        {
            this.element.findCache('.character_icon').empty();
        }
    }

    static getTemplateElement(){
        return CssCharacter.templateElement.clone();
    }

    static setCharacterTemplateElement(template){
        CssCharacter.templateElement = template;
    }
    static setCharacterTemplateContainer(template){
        CssCharacter.templateContainer = template;
    }

}
CssCharacter.characters = {};
CssCharacter.visiblePlayerTypes = {
    human: true,
    cpu: true,
}

module.exports = CssCharacter;