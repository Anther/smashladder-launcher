class ObjectSort
{

	static sortByProperty(object, property){
		let list = [];
		for(let i in object)
		{
			if(!object.hasOwnProperty(i))
			{
				continue;
			}
			let element = object[i];
			list.push(element);
		}
		list.sort(function(a,b){
			return a[property] - b[property];
		});
		return list;
	}

	static sortByPropertyWithCallback(object, callback){
		let list = [];
		for(let i in object)
		{
			if(!object.hasOwnProperty(i))
			{
				continue;
			}
			let element = object[i];
			list.push(element);
		}
		list.sort(function(a,b){
			return callback(a) - callback(b);
		});
		return list;
	}
}

module.exports = ObjectSort;