class MemoryCache
{
    constructor(processor){
        this.memory = {};
        this.processor = processor;
    }

    cacheValue(name, value){
        if(typeof this.memory[name] !== 'undefined')
        {
            if(this.memory[name] === value)
            {
                return false;
            }
        }
        this.memory[name] = value;
        this.processor.log('has change', name, value);
        // this.emitValue(name);
        this.processor.hasChanges = true;
        return true;
    }

    get(key){
        return this.memory[key];
    }

    allData(){
        let data = {};
        for(let i in this.memory){
            if(!this.memory.hasOwnProperty(i))
            {
                continue;
            }
            data[i] = this.memory[i];
        }
        return data;
    }

    arrayBufferToString(buffer){
        var arr = new Uint8Array(buffer);
        var values = [];
        arr.forEach((element)=>{
            values.push(element);
        });
        console.log(values);
    }
}

module.exports = MemoryCache;