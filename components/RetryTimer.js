class RetryTimer{
    constructor(action, name){
        this.name = name;
        this.action = action;
        if(typeof action !== "function")
        {
            throw 'Invalid Timer Value!';
        }
        this.finishPromise = null;
    }
    setTimer(time){
        this.cancel();
        this.finishPromise =
            setTimeout(() => {
                console.log('RETRY TIMER', this.name, this.action);
                try{
                    this.action();
                }catch(e)
                {
                    console.trace(e);
                    throw e;
                }
                this.finishPromise = null;
            }, time);
    }
    cancel(){
        if(this.finishPromise)
        {
            clearTimeout(this.finishPromise);
        }
    }
    end(){
        if(this.finishPromise)
        {
            clearTimeout(this.finishPromise);
            this.action();
        }
    }
    isActive(){
        return !!this.finishPromise;
    }
}

module.exports = RetryTimer;