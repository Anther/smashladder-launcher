const MemoryCache = require('./MemoryCache');
const StatPlayer = require('./StatPlayer');

class StatProcessor
{
    constructor(puller){
        this.puller = puller;
        this.hasChanges = false;

        this.memoryCache = new MemoryCache(this);
        this.players = new Map();
        this.activePlayers = new Map();
        for(var i = 1; i <= 4; i++){
            this.players.set(i,new StatPlayer(i, this));
        }
    }

    setServer(server){
        this.server = server;
    }

    process(stat){
        if(stat.condition && stat.condition(this) === false)
        {
            return Promise.resolve();
        }
        return this.puller.memoryProcessor.readProcessMemory(stat.address, stat.length)
            .then((view)=>{
                return stat.process(this, view);
            });
    }

    log(){
        return;
        console.log(arguments);
    }

    matchIsNotActive(){
        return (this.memoryCache.get('menu') === 129);
    }

    matchIsPaused(){
        return this.memoryCache.get('paused') == true;
    }

    cacheValue(name, value){
        return this.memoryCache.cacheValue(name, value);
    }

    clearActiveMatchData(){
        this.matchIsActive = false;
    }

    emitIfChanges(){
        if(this.hasChanges)
        {
            this.hasChanges = false;
            this.emitAll();
        }
        else
        {
        }
    }

    emitAll(){
        var output = {};

        output.players = {};
        var playerCount = 0;
        if(this.matchIsNotActive())
        {
            this.clearActiveMatchData();
        }
        let playersWithStocks = 0;
        for(let [i, player] of this.activePlayers){
            if(this.matchIsNotActive())
            {
                // player.reset();
                // continue;
            }
            if(!player.isActive)
            {
                throw new Error('this should not happen');
            }
            if(player.stats.x === null)
            {
                player.setActive(false);// So that we can emit their final stock stats
            }
            else
            {
                playerCount++;
            }
            if(player.stats.stocks > 0)
            {
                playersWithStocks++;
            }
            output.players[player.slot] = player.stats;
        }
        output.cssPlayers = {};
        for(let[i,player] of this.players){
            output.cssPlayers[player.slot] = {
                cssCharacter: player.stats.cssCharacter,
                costume: player.stats.costume,
                playerType: player.stats.playerType,
            };
        }
        if(playerCount <= 1)
        {
            this.clearActiveMatchData();
        }
        output.game = this.memoryCache.allData();
        if(!this.matchIsActive)
        {
            output.game.timeRemaining = null;
            output.game.stage = null;
        }
        if(playersWithStocks <= 1)
        {
            //Should eventually be a check for "Teams with stocks"
            output.game.paused = false;
            output.game.pausedBy = null;
            output.game.matchIsOver = true;
        }
        // console.log(output);
        this.server.emit('matchUpdate', output);
        this.hasChanges = false;
        this.matchIsActive = true;
    }
}

module.exports = StatProcessor;