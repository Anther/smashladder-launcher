class StatPlayer{

    setActive(value){
        if(value === this.isActive)
        {
            return;
        }
        this.isActive = value;
        if(value)
        {
            // this.puller.log('setting active!', this.slot);
            this.processor.activePlayers.set(this.slot, this);
        }
        else
        {
            // this.processor.log('setting inactive!', this.slot);
            this.processor.activePlayers.delete(this.slot);
        }
    }

    constructor(slot, processor){
        this.slot = slot;
        this.processor = processor;
        this.addressIndex = slot - 1;
        this.setActive(false);

        this.stats = {
            stocks: null,
            suicides: null,
            kills: null,
            damage: null,
            action: null,
            x: null,
            y: null
        };

    }

    reset(){
        for(var i in this.stats)
        {
            if(!this.stats.hasOwnProperty(i))
            {
                continue;
            }
            this.stats[i] = null;
        }
    }

    updateStat(name, value){
        if(StatPlayer.statCallbacks[name])
        {
            value = StatPlayer.statCallbacks[name](value);
        }
        if(StatPlayer.statEquals[name])
        {
            if(StatPlayer.statEquals[name](value, this.stats[name]))
            {
                return false;
            }
        }
        else
        {
            if(this.stats[name] === value)
            {
                return false;
            }
        }

        this.stats[name] = value;
        if(this.isActive)
        {
            this.processor.hasChanges = true;
        }
        if(StatPlayer.statsThatTriggerChangeAnyway[name])
        {
            this.processor.hasChanges = true;
        }
        return true;
    }
}
StatPlayer.statsThatTriggerChangeAnyway = {
    cssCharacter: true,
    cssHover: true,
    playerType: true,
    costume: true,
};
StatPlayer.playerTypes ={
    0: 'human',
    1: 'cpu',
    2: 'demo?',
    3: 'disabled'
};

StatPlayer.statCallbacks = {
    character: (value)=>{
        if(value > 26)
        {
            return null;
        }
        return value;
    },
    playerType: (value)=>{
        return StatPlayer.playerTypes[value];
    }
};
StatPlayer.statEquals = {
    specificKills:(one, two) =>{
        if(!one || !two)
        {
            return false;
        }
        for(var i in one){
            if(!one.hasOwnProperty(i))
                continue;
            if(two[i] !== one[i])
            {
                return false;
            }
        }
        return true;
    }
};

module.exports = StatPlayer;