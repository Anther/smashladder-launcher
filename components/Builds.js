const $ = require('jquery');
const settings = require('./AppSettings.js');
var constants = require("./constants.js");
const SmashladderApi = require("./SmashladderApi.js");
const Build = require("./Build.js");
const Notification = require("./Notification.js");
const LadderGame = require("./LadderGame.js");
const LadderMap = require("./helpers/LadderMap.js");
var ObjectSort = require("./helpers/ObjectSort.js");

class Builds
{
    constructor(builds){
        this.builds = {};
        Builds.instance = this;

        for(var i in builds)
        {
            if(!builds.hasOwnProperty(i))
            {
                continue;
            }
            var build = builds[i];
            this.builds[i] = this.createNewBuild(build);
        }
    }

    createNewBuild(buildData){
        var build = new Build(buildData);
        build.setBuilds(this);
        return build;
    }

    populateGameLists(){
        for(let i in this.builds){
            if(!this.builds.hasOwnProperty(i))
            {
                continue;
            }
            let build = this.builds[i];
            let element = build.getElement();
            let selectElement = element.findCache('select[name=game_name]');
            LadderGame.getGamesSortedForBuild(build).forEach((game)=>{
                if(selectElement.find('.game_'+game.getId()).length)
                {
                    console.log('already populated');
                    return;
                }
                // console.log(game.getHint());
                let option = $('<option>').attr('value', game.getHint()).text(game.getTitle()).addClass('game_'+game.getId());
                selectElement.prepend(option);
                option.data('game', game);
            });
            selectElement.val(selectElement.find('option:first').val());
        }
    }

    updateFromServer(serverBuilds){
        var i;
        for(let i in serverBuilds)
        {
            if(!serverBuilds.hasOwnProperty(i))
            {
                continue;
            }
            var build = serverBuilds[i];
            if(this.builds[build.dolphin_build_id])
            {
                this.builds[build.dolphin_build_id].update(build);
            }
            else
            {
                this.builds[build.dolphin_build_id] = this.createNewBuild(build);
                this.builds[build.dolphin_build_id].updateElement().css('opacity', 1);
            }
        }
        for(i in this.builds){
            if(!this.builds.hasOwnProperty(i))
            {
                continue;
            }
            if(!serverBuilds[i])
            {
                console.log('build missing', this.builds[i]);
                var oldElement = this.builds[i];
                delete this.builds[i];
                oldElement.remove();
            }
        }
        let sortedBuilds = this.getBuildsSorted();
        for(let build of sortedBuilds)
        {
            this.appendElement(build.getElement());
        }
    }

    highlightBuild(name){
        if(!this.hasBuild[name])
        {
            return;
        }
        for(var i in this.builds)
        {
            if(!this.builds.hasOwnProperty(i))
            {
                continue;
            }
            var build = this.builds[i];
            if(!build || !build.element)
            {
                continue;
            }
            if(build.element.hasClass('opened'))
            {
                continue;
            }
            if(i == name)
            {
                build.element.removeClass('highlighted');
            }
            else
            {
                build.element.addClass('highlighted');
            }
        }
    }

    getBuilds(){
        return this.builds;
    }

    getBuildsSorted(){
        return ObjectSort.sortByProperty(this.builds, 'order');
    }

    getFirstBuild(){
        let build = this.getBuildsSorted();
        if(build.length)
        {
            return build[0];
        }
        return null;
    }

    hasBuild(name){
        return this.builds.hasOwnProperty(name);
    }

    serialize(){
        var saveBuilds = {};
        for(var buildName in this.builds)
        {
            if(!this.builds.hasOwnProperty(buildName))
            {
                continue;
            }
            saveBuilds[buildName] = this.builds[buildName].saveSerialize();
        }
        return saveBuilds;
    }

    save(){
        let saveBuilds = this.serialize();
        return settings.set('allDolphinBuilds', saveBuilds)
            .catch((e)=>{
                throw e;
            });
    }

    syncToServer(){
        var buildData = {};
        for(var i in this.builds){
            if(!this.builds.hasOwnProperty(i))
            {
                continue;
            }
            var build = this.builds[i];
            buildData[build.id] = build.syncSerialize();
        }
        return SmashladderApi.apiv1Post(constants.apiv1Endpoints.SYNC_BUILDS, {builds: buildData})
            .catch((error)=>{
                throw (error);
            });
    }

    static retrieveActiveBuilds() {
        let dolphinBuildsVersion = settings.getSync('dolphinBuildsVersion');
        if(dolphinBuildsVersion != 2)
        {
            settings.set('allDolphinBuilds', {});
            settings.set('dolphinBuildsVersion', 2);
            settings.set('ladderGames', {});
        }
        return settings.get('allDolphinBuilds')
            .then((builds) => {
                for (var i in builds) {
                    if (builds.hasOwnProperty(i)) {
                        var allBuilds = new Builds(builds);
                        Builds.fetchBuilds(allBuilds);//Get Updated list also
                        return allBuilds;
                    }
                }
                return Builds.fetchBuilds();
            }).catch((error) => {
                throw error;
            });
    };

    static fetchBuilds(previousAllBuilds) {
        return SmashladderApi.apiv1Get(constants.apiv1Endpoints.DOLPHIN_BUILDS)
            .then((json) => {
                console.log('fetched', json);
                if (json.builds) {
                    var hasBuilds = false;
                    let buildList = {};
                    let ladderList = new LadderMap();
                    for (var ladderId in json.builds) {
                        if (!json.builds.hasOwnProperty(ladderId)) {
                            continue;
                        }
                        hasBuilds = true;
                        let ladderGameData = json.builds[ladderId];
                        let ladderGame = LadderGame.updateGame(ladderGameData);
                        ladderList.set(ladderId, ladderGame.serialize());
                        for(var buildIndex in ladderGameData.builds){
                            if(!ladderGameData.builds.hasOwnProperty(buildIndex))
                            {
                                continue;
                            }
                            let build = ladderGameData.builds[buildIndex];
                            //TODO: Some builds are not getting this set (Dolphin 5.0-321)
                            if(!buildList[build.dolphin_build_id])
                            {
                                buildList[build.dolphin_build_id] = build;
                                buildList[build.dolphin_build_id].forLadders = {};
                            }
                            // buildList[build.dolphin_build_id] = build; //Isn't this redundant...?
                            buildList[build.dolphin_build_id].forLadders[ladderId] = true;
                        }
                    }
                    if (hasBuilds) {
                        if(previousAllBuilds)
                        {
                            previousAllBuilds.updateFromServer(buildList);
                            return previousAllBuilds;
                        }
                        else
                        {
                            return new Builds(buildList);
                        }
                    }
                    throw 'Json builds not found';
                }
                else {
                    console.log('error retrieving');
                }

            }).then((builds) => {
                builds.save();
                return builds;
            });
    }

    getBuildById(id){
        // console.log('all builds', this.builds);
        return this.builds[id];
    };

    static startGame(id){
        const build = Builds.instance.getBuildById(id);
        build.startGame();
    }

    static sendChatMessage(id, message){
        const build = Builds.instance.getBuildById(id);
        build.sendChatMessage(message);
    }

    static launchDolphinBuildById(id, data, closePrevious){
        const build = Builds.instance.getBuildById(id);


        build.host_code = data && data.parameters && data.parameters.host_code ? data.parameters.host_code : null;
        build.hostId = (data && data.data.host_id) ? data.data.host_id : null;

        var launchName = (data && data.data.game_launch_name) ? data.data.game_launch_name : null;

        if(!build)
        {
            Notification.showFocusedAlert('Build not found...');
            return;
        }
        var launched;
        if(build.host_code)
        {
            launched = build.join(build.host_code, closePrevious);
        }
        else
        {
            if(launchName)
            {
                settings.set('lastLaunch', launchName);
            }
            launched = build.host(launchName, closePrevious);
        }
        return launched
            .catch(error=>{
                console.error(error);
            });
    }

    clearBuilds() {
        var builds = this.getBuilds();
        for(var i in builds)
        {
            if(!builds.hasOwnProperty(i))
            {
                continue;
            }
            builds[i].remove();
        }
        this.builds = {};

        return settings.set('allDolphinBuilds', {})
            .then( ()=>{

            }).catch((e)=>{
                throw e;
            });
    }
    
    getTemplateElementForBuild(build){
        var element = Builds.templateBuildElement.clone();
        element.attr('id', 'build_'+build.dolphin_build_id);
        this.appendElement(element);
        return element;
    }

    appendElement(element){
        element.appendTo(Builds.buildsContainer);
    }
}

Builds.instance = null;
Builds.templateBuildElement = null;
Builds.buildsContainer = null;

module.exports = Builds;