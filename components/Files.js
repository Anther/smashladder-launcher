"use strict";

const electron = require('electron');
const dialog = electron.dialog;
const app = electron.app;
const fs = require('fs');
const path = require('path');

class Files {


    static selectDolphinLocation(mainWindow){
        var files = dialog.showOpenDialog(mainWindow, {
            properties: ['openFile'],
            filters: [
                {name: 'Dolphin Exe', extensions: ['exe']}
            ]
        });

        if(!files){
            return;
        }

        var file = files[0];
        app.addRecentDocument(file);

        return file;
    }

    static findInDirectory(startPath, filter, callback){
        var results = [];

        if (!fs.existsSync(startPath)){
            throw new Error('Start directory not found '+startPath);
        }

        var files=fs.readdirSync(startPath);
        for(var i=0;i<files.length;i++){
            var filename=path.join(startPath,files[i]);
            var stat = fs.lstatSync(filename);
            if (stat.isDirectory()){
                results = results.concat(Files.findInDirectory(filename,filter)); //recurse
            }
            else if (filename.indexOf(filter)>=0) {
                console.log('-- found: ',filename);
                results.push(filename);
            }
        }
        return results;
    }

}


module.exports = Files;
