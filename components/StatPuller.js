const MemoryAddresses = require('./MemoryAddresses');
const StatProcessor = require('./StatProcessor');
const StringManipulator = require('./StringManipulator');
//https://docs.google.com/spreadsheets/d/1JX2w-r2fuvWuNgGb6D3Cs4wHQKLFegZe2jhbBuIhCG8/edit#gid=12
class StatPuller {

    constructor(memoryProcessor){
        if(!memoryProcessor)
        {
            throw new Error('Memory processor is necessary!');
        }

        this.statProcessor = new StatProcessor(this);
        this.memoryAttached = false;
        this.memoryProcessor = memoryProcessor;
        this.statProcessor.memoryProcessor = memoryProcessor;
    }

    setServer(server){
        this.server = server;
        this.statProcessor.setServer(server);
        this.server.on('connect', ()=>{
            this.log('CONNECTION');
            this.statProcessor.emitAll();
        })
    }

    


    start(){
        if(this.running)
        {
            return;
        }
        else if(this.memoryAttached === false)
        {
            console.log('attaching');
            this.memoryAttached = null;
            return this.memoryProcessor.attach()
                .then(()=>{
                    this.memoryAttached = true;
                    this.log('attached');
                    this.start();
                }).catch(()=>{
                    this.memoryAttached = false;
                    this.log('failed to attach');
                    setTimeout(()=>{
                        this.log('retry');
                        this.start();
                    },1000);
                });
        }
        else if(this.memoryAttached === null)
        {
            this.log('attempting attachment');
            return;
        }

        // setInterval(()=>{
        //     let memoryUsage = process.memoryUsage();
        //     console.log('------');
        //     for(var i in memoryUsage){
        //         console.log(i,StringManipulator.humanFileSize(memoryUsage[i], true) );
        //     }
        // },4000)

        this.stop();
        this.running = true;
        this.loopNumber = 0;
        this.loopTimeLimit = 30;
        this.defaultLoopTimeLimit = 8;
        this.defaultLoopTimeLimit = 8;
        var loopRunner = ()=>{
            if(!this.running || ( this.promiseActive ))
            {
                //Process memory hangs so we just have to keep firing
                this.log('another loop was running');
                return;
            }
            this.log('current time limit ', this.loopTimeLimit);
            this.loopNumber++;
            this.promiseActive = true;
            this.log('waiting');
            this.all(this.loopTimeLimit).then(()=>{
                this.log('success');
                this.promiseActive = false;
                this.loopTimeLimit = this.defaultLoopTimeLimit;
                setTimeout(loopRunner, this.loopTimeLimit);
            }).catch((error)=>{
                console.trace(error);
                if(this.loopTimeLimit < 20000)
                {
                    this.loopTimeLimit += 200
                }
                this.promiseActive = false;
                setTimeout(loopRunner, this.loopTimeLimit);
            });
        };
        setTimeout(loopRunner, this.loopTimeLimit);
    }

    stop(){
        this.running = false;
    }

    log(){
        return;
        console.log(arguments);
    }

    all(){
        return new Promise((resolve,reject)=>{
            let timedOut = false;
            let timeout = setTimeout(()=>{
                timedOut = true;
                reject(new Error('Took too long to execute'));
            }, this.loopTimeLimit);


            // this.matchPlayerStats();
            this.timeRemaining()
                // .then(()=>{
                //     return this.testing()
                // })
                .then(()=>{
                    return this.paused()
                })
                .then(()=>{
                    return this.pausedByCameraInfo()
                })
                .then(()=>{
                    return this.stage()
                })
                .then(()=>{
                    // return true;
                    return this.matchPlayerStats(); //also causing memory leak
                })
                .then(()=>{
                    return this.characterSelect();
                })
                .then(()=>{
                    return this.menu()
                })
                .then(()=>{
                    clearTimeout(timeout);
                    if(timedOut)
                    {
                        return false;
                    }
                    this.statProcessor.emitIfChanges();
                    resolve();
                })
                .catch((error)=>{
                    clearTimeout(timeout);
                    console.trace(error);
                    if(timedOut)
                    {
                        return false;
                    }
                    this.statProcessor.emitIfChanges();
                    resolve();
                });
        })
    }



    readMemory(stat) {
        return this.statProcessor.process(stat);
    }

    paused(){
        return this.readMemory(MemoryAddresses.paused);
    }

    pausedByCameraInfo(){
        return this.readMemory(MemoryAddresses.pausedByCameraInfo);
    }

    timeRemaining(){
        return this.readMemory(MemoryAddresses.timeRemaining)
    }

    stage(){
        return this.readMemory(MemoryAddresses.stage);
    }


    menu(){
        return this.readMemory(MemoryAddresses.menu)
    }

    globalFrameCounter(){
        return this.readMemory(MemoryAddresses.globalFrameCounter)
    }

    characterSelect(){
        this.log('css');
        return this.readMemory(MemoryAddresses.characterSelect)
    }

    matchPlayerStats(){
        return this.readMemory(MemoryAddresses.matchPlayerStats);
    }

}

module.exports = StatPuller;