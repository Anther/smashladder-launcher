const MemoryAddresses = {
    testing: {
        address: 0,
        length: 4,
        process: (processor, view)=> {
            console.log('hmm', processor, view);
        }
    },
    paused: {
        address: 0x479D68,
        length: 4,
        process: (processor, view)=> {
            let value = view.getUint8(0);
            processor.cacheValue('paused', value ? true : false);
        }
    },
    pausedByCameraInfo: {
        address: 0x46b6A1,
        length: 1,
        condition: (processor)=>{
            if(!processor.matchIsPaused())
            {
                processor.cacheValue('pausedBy', null);
                return false
            }
        },
        process: (processor, view)=> {
            let value = view.getUint8(0);
            processor.cacheValue('pausedBy', value + 1);
        }
    },
    pausedByMatchInfo: {
        address: 0x452F2D,
        length: 1,
        condition: (processor)=>{
            if(!processor.matchIsPaused())
            {
                processor.cacheValue('pausedBy2', null);
                return false
            }
        },
        process: (processor, view)=> {
            let value = view.getUint8(0);
            processor.cacheValue('pausedBy2', value + 1);
        }
    },

    globalFrameCounter: {
        address: 0x479D60,
        length: 4,
        process: (processor, view)=>{
            console.log('GFC');
            console.log('8', view.getUint8(0));
            console.log('32', view.getUint16(0));
            console.log('32', view.getUint32(0));
        }
    },
    timeRemaining:{
        address: 0x46B6C8,
        length: 4,
        condition: (processor)=>{
            if(processor.matchIsNotActive())
            {
                processor.cacheValue('timeRemaining', null);
                return false
            }
        },
        process: (processor, view) =>{
            let value = view.getUint32(0);
            processor.cacheValue('timeRemaining', value)
        }
    },
    gameTimeCounter:{
        address: 0x46B6CC,
        length: 2,
        process: (processor, view) =>{
            let value = view.getUint16(0);
            processor.cacheValue('gameFrames', value)
        }
    },
    menu:{
        address: 0x65CC14,
        length: 1,
        process: (processor, view) =>{
            let value = view.getUint8(0);
            processor.cacheValue('menu', value);
        }
    },
    characterSelect:{
        address: 0x3F0E08,
        length: 0x90,
        individualLength: 0x24,
        process: (processor, view)=>{
            for(let [playerIndex, player] of processor.players)
            {
                let playerOffset = MemoryAddresses.characterSelect.individualLength * player.addressIndex;
                player.updateStat('playerType', view.getUint8(playerOffset));
                player.updateStat('costume', view.getUint8(playerOffset + 1));
                player.updateStat('cssCharacter', view.getUint8(playerOffset + 2));
                player.updateStat('cssHover', view.getUint8(playerOffset + 3));
            }
        }
    },
    stage:{
        address: 0x49E6C8,
        length: 0x742,
        condition: (processor)=>{
            return true;
            if(processor.matchIsNotActive())
            {
                processor.cacheValue('stage', null);
                return false;
            }
        },
        process: (processor, view) =>{
            let value = view.getUint32(0x88);
            processor.cacheValue('stage', value);
        }
    },
    matchPlayerStats:{
        address: 0x453080,
        length: 0x3A40,
        lengthBetweenPlayers: 0xE90,
        playerEntityPointerLocation: 0xB0,
        process: (processor, view) =>{
            if(processor.memoryCache.get('stage'))
            {
                let allRequests = [];
                for(let [playerIndex, player] of processor.players)
                {
                    let playerEntityPointer = (0xE90 * player.addressIndex) + MemoryAddresses.matchPlayerStats.playerEntityPointerLocation;
                    let playerAddress = view.getUint32(playerEntityPointer);
                    if(playerAddress === 0){
                        //Pointer is pointing to nothing
                        player.setActive(false);
                        continue;
                    }
                    player.setActive(true);
                    let playerEntityRequest = processor.memoryProcessor.readProcessMemory(playerAddress - 0x80000000, 0x48).then((playerEntityView) =>{
                        let playerCharacterDataAddress = 0x2C;
                        let playerData = playerEntityView.getUint32(playerCharacterDataAddress);
                        return processor.memoryProcessor.readProcessMemory(playerData - 0x80000000, 0x110).then((playerView) =>{
                            player.updateStat('x', playerView.getFloat32(0xB0));
                            player.updateStat('y', playerView.getFloat32(0xC0) + playerView.getFloat32(0xCC));
                            player.updateStat('action', playerView.getUint32(0x10));
                        }).catch((error)=>{
                            player.updateStat('x', null);
                            player.updateStat('y', null);
                            player.updateStat('action', null);
                        });
                    }).catch((error)=>{
                        throw error;
                    });
                    allRequests.push(playerEntityRequest);
                }
            }
            for(let [playerIndex, player] of processor.players)
            {
                let playerOffset = 0xE90 * player.addressIndex;
                try{
                    player.updateStat('suicides', view.getUint16((playerOffset) + 0x8C));
                    player.updateStat('stocks', view.getUint8((playerOffset) + 0x8E));
                    player.updateStat('character', view.getUint32((playerOffset) + 0x4));
                    player.updateStat("damage", Math.floor(view.getFloat32((playerOffset) + 0xD28)));
                    player.updateStat("damageReceived", Math.floor(view.getFloat32((playerOffset) + 0xD1C)));
                    player.updateStat("attackCount", Math.floor(view.getUint32((playerOffset) + 0xE8)));
                    player.updateStat("playerId", Math.floor(view.getFloat32((playerOffset) + 0x48)));
                }
                catch(error){
                    processor.log('Stat outside of memory bounds')
                }

                let totalKills = 0;
                let specificKills = {};

                let distancePerKillAddress = 0x4;
                let killsBeginningAddress = 0x70;
                for(let j = 0; j < 4; j++){
                    let slot = j+1;
                    let killsOffset = (playerOffset) + killsBeginningAddress + (distancePerKillAddress * j);
                    let kills = view.getUint32(killsOffset);
                    specificKills[slot] = kills;
                    totalKills += kills;
                }
                player.updateStat('specificKills', specificKills);
                player.updateStat('kills', totalKills);
            }
        }
    },
    controller:{
        address: 0x4C1FAC,
        length: 0xEC
    }

};

module.exports = MemoryAddresses;