const StdOutSplitter = require("./StdOutSplitter.js");
const DolphinChecker = require("./DolphinChecker.js");
const constants = require("./constants.js");
const path = require('path');
const child = require('child_process');
const net = require('net');

class SendChatInjector {
	constructor(){
		this.debugInterval = false;
		this.debugLog = false;

		this.outputStream = null;
		this.dolphinIsInjected = false;
		this.sendChatLocation = path.join(constants.root, '/external/sendChatInject/ChaseInjector.exe');

		this.pipePath = "\\\\.\\pipe\\" + "LadderMessagePipe";
	}

	async postMessage(message = null){
		if(!message)
		{
			return Promise.reject(new Error('Message was empty...'));
		}
		let dolphinIsInjected = await this._injectDolphin();
		if(!dolphinIsInjected)
		{
			return Promise.reject(new Error('Dolphin is not currently running'));
		}
		return new Promise((resolve)=>{
			this._createServer().then(()=>{
				if(this.outputStream)
				{
					this.log('sent: ' + message);
					this.outputStream.write(message);
					return resolve();

				}
			});

		}).catch((error)=>{
			this.logError(error);
		});
	}

	_injectDolphin(){
		if(!DolphinChecker.dolphinIsRunning())
		{
			return Promise.resolve(false);
		}
		if(this.dolphinIsInjected)
		{
			return Promise.resolve(true);
		}
		return new Promise((resolve, reject)=>{

			this.chatInjectStarter = child.spawn(this.sendChatLocation, {
				cwd: path.dirname(this.sendChatLocation)
			});
			this.chatInjectStarter.stdout.on('data', (data) =>{
				let output = StdOutSplitter.parse(data);
				for(let string of output)
				{
					if(string.includes('DLL Injected successfully'))
					{
						this.log('we got a success message from inject0r');
						resolve(true);
						break;
					}
				}
			});
			this.chatInjectStarter.on('close', (data) =>{
				this.log('resolve after close');
				this.dolphinIsInjected = true;
				resolve(true);
			});

		});
	}

	/**
	 *
	 * Returns a promise that resolves when the server is writable
	 */
	_createServer(){
		if(this.outputStream)
		{
			return Promise.resolve();
		}
		return new Promise((resolve, reject)=>{
			let server = net.createServer((stream) =>{
				this.log('Server: on connection');
				this.outputStream = stream;

				stream.on('data', (serverData) =>{
					this.log('Server: on data:', serverData.toString());
				});

				stream.on('end', () =>{
					this.outputStream = null;
					this.log('Server: on end');
					server.close();
					this.dolphinIsInjected = false;
				});

				if(this.debugInterval)
				{
					let interval = setInterval(()=>{
						if(!this.outputStream)
						{
							clearInterval(interval);
						}
						this.outputStream.write('rawr');
					}, 1000);
				}
				resolve();
			});

			server.on('close', ()=>{
				this.outputStream = null;
				this.log('Server: on close');
				reject();
			});


			server.listen(this.pipePath, ()=>{
				this.log('Server: on listening');
			});

			server.on('error', (error)=>{
				this.logError('pipe error', error);
				server.close();
				this.dolphinIsInjected = false;
				reject();
			});
		});
	}

	log(){
		if(this.debugLog)
		{
			console.log.apply( this, arguments );
		}
	}

	logError(){
		if(this.debugLog)
		{
			console.error.apply( this, arguments);
		}
	}

}

module.exports = SendChatInjector;