const $ = require('jquery');
$.fn.findCache = function(selector){
    var element = this;

    if(!this.data('findCache'))
    {
        this.data('findCache', {});
    }
    if(!this.data('findCache'))
    {
        console.error('UNDEFINED ELEMENT', this, selector);
        return $();
    }
    if(this.data('findCache')[selector])
    {
        return this.data('findCache')[selector];
    }
    else
    {
        var result = this.find(selector);
        this.data('findCache')[selector] = result;
        return result;
    }
};

$.fn.cacheText = function(value){
    var element = this;
    if(element.data('cacheDisplay') !== value){
        return element.data('cacheDisplay', value).text(value);
    }
    return element;
};

$.fn.findCacheDisplayText = function(selector, value){
    var element = this;
    return this.findCache(selector).cacheText(value);
};
module.exports = $;