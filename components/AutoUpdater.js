const {autoUpdater} = require('electron');
const fs = require('fs');
const path = require('path');
const constants = require('./constants');

if(!fs.existsSync(path.resolve(path.dirname(process.execPath), '..', 'Update.exe')))
{
    return;
}

var updateFeed = constants.APP_UPDATES_URL;

autoUpdater.setFeedURL(updateFeed);
autoUpdater.checkForUpdates();

console.log('feed url = ' + autoUpdater.getFeedURL());


autoUpdater.on('error', function(err){
    console.log('there has been an error');
    console.log(err);
});

autoUpdater.on('checking-for-update', function(){
    console.log('checking');
});

autoUpdater.on('update-available', function(){
    console.log('available');

});

autoUpdater.on('update-not-available', function(){
    console.log('not available');

});

autoUpdater.on('update-downloaded', function(){
    console.log('downloaded');

    autoUpdater.quitAndInstall();
});