
const constants = require('./constants');
const JsonHelper = require('./JsonHelper');
const fetch = require('node-fetch');
const request = require('request-promise-native');
var Authentication = require("./Authentication.js");

var ClientOAuth2 = require('client-oauth2');

class SmashladderApi
{

    static isObject(obj) {
        return obj === Object(obj);
    }

    static apiGoGoGo(endpoint, formData, method, useAuthentication = true){
        return DebugPause(2000).then(()=>{
            if(useAuthentication)
            {
                return Authentication.load();
            }
            else
            {
                return null;
            }
        }).then((authentication)=>{
            let requestData = {};
            let url = constants.SITE_URL+'/api/v1/'+endpoint;
            if(useAuthentication)
            {
                let oauthAuth = new ClientOAuth2({});
                if(!authentication.getAccessCode())
                {
                    throw new Error('Invalid Login Code');
                }
                let token = oauthAuth.createToken(authentication.getAccessCode());

                requestData = token.sign({
                    method: method,
                    url: url,
                    form: formData,
                });
            }
            else
            {
                requestData.method = method;
                requestData.url = url;
                requestData.form = formData;
            }
            console.log('['+method+']', endpoint, formData);
            return request(requestData);
        }).then((success)=>{
            let parsed = JSON.parse(success);
            return parsed;
        },(error)=>{
            console.log('server error or something');
            console.error(endpoint);
            throw error;
        }).catch(error =>{
            if(error && error.statusCode)
            {
                
            }
            throw error;

        });
    }

    static apiv1Post(endpoint, formData, useAuthentication = true){
        return SmashladderApi.apiGoGoGo(endpoint, formData, 'POST', useAuthentication);
    }

    static apiv1Get(endpoint, formData, useAuthentication = true){
        return SmashladderApi.apiGoGoGo(endpoint, formData, 'GET', useAuthentication);
    }

    static post(url, data = {}, useAuthentication = null) {
        throw new Error('Completely Deprecated');
    }

}


function DebugPause(time) {
    if(!constants.debugUrls || true)
    {
        return Promise.resolve();
    }
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve();
        }, time);
    });
}

SmashladderApi.authentication = null;

module.exports = SmashladderApi;