"use strict";

const FormData = require('form-data');
const constants = require('./constants');
const fetch = require('node-fetch');

const settings = require('./AppSettings');


class Authentication{
    constructor(loginCode, player, session_id){
        this.loginCode = loginCode;
        this.player = player;
        this.session_id = session_id;
    }

    _parseCredentials(){
        var atob = require('atob');
        let loginCredentials = this.loginCode;
        let string = atob(loginCredentials);
        let split = string.split(':');
        return {
            access: split[1]
        };
    }
    
    getAccessCode(){
        let credentials = this._parseCredentials();
        return credentials.access;
    }
    
    checkAuthentication(){
        var SmashladderApi = require("./SmashladderApi.js");

        if(this.checkValid)
        {
            console.log('[SHORTCUT CHECK]');
            return Promise.resolve(this);
        }

        if(this.loginCode)
        {
            var data = {};
        }
        else
        {
            throw {no_player: 'rawr'};
        }

        return SmashladderApi.apiv1Get(constants.apiv1Endpoints.PLAYER_PROFILE, data)
            .then(response => {
                this.player = response.player;
                this.session_id = response.session_id;
                console.log('{the player?}', response.player);
                return this.save();
            }).then(()=>{
                return this;
            }).catch(error=>{
                throw error;
            });
    }

    static load(){
        return settings.get('userAuthentication')
            .then((authentication) =>{
                if(!authentication)
                {
                    authentication = {};
                }
                return new Authentication(authentication.loginCode, authentication.player, authentication.session_id);
            }).catch(function(error){
                throw error;
            });
    }

    save(){

        var authenticationData = {
            player: this.player,
            session_id: this.session_id,
            loginCode: this.loginCode
        };
        return settings.set('userAuthentication', authenticationData)
            .then(() =>{
                return authenticationData;
            }).catch(error=>{
                throw error;
            });
    }
    static deleteAuthentication(){
        return settings.set('userAuthentication', {})
            .catch(function(error){
                throw error;
            });
    }
}
module.exports = Authentication;