

class StdOutSplitter
{
	static parse(data)
	{
		if(!data)
		{
			return [];
		}
		var strings = data.toString().split(/\r?\n/);
		return strings;
	}
}

module.exports = StdOutSplitter;