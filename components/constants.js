"use strict";

require ('hazardous');
const electron = require('electron');
var constants = {};

constants.root = require('app-root-path').toString();
var info = require('../package.json');

constants.version = info.version;

constants.debugUrls = false;
constants.debugConsole = false;

constants.showMemoryScannerProcess = false;
constants.debuggingMatchInputs = constants.debugUrls;

var hosts = {
    dev: function(){
        constants.SITE_URL = 'http://localhost/smashladder';
        constants.WEBSOCKET_URL = 'ws://localhost:100';
        constants.APP_UPDATES_URL = 'http://localhost/smashladder/downloads/launcher/';
    },
    live: function(){
        constants.SITE_URL = 'https://www.smashladder.com';
        constants.WEBSOCKET_URL = 'wss://www.smashladder.com';
        constants.APP_UPDATES_URL = 'https://www.smashladder.com/downloads/launcher/';
    }
};

if(constants.debugUrls)
{
    hosts.dev();
}
else
{
    hosts.live();
}

constants.apiv1Endpoints = {
    PLAYER_PROFILE:             'player/me',
    DOLPHIN_BUILDS:             'dolphin/all_builds',
    SYNC_BUILDS:                'dolphin/sync_builds',
    CLOSED_DOLPHIN:             'dolphin/closed_host',
    OPENED_DOLPHIN:             'dolphin/opened_dolphin',
    UPDATE_BUILD_PREFERENCES:   'matchmaking/update_active_build_preferences',
    DOLPHIN_HOST:               'dolphin/set_host_code',
    REPORT_MATCH_GAME:          'dolphin/report_match_game',
    RETRIEVE_MATCH_GAME_ID:     'dolphin/prepare_match_game',
    DOLPHIN_PLAYER_JOINED:      'dolphin/player_list_update',
    LOGOUT:                     'player/logout',
};

constants.getUrl = function(path){
    return constants.SITE_URL + path;
};
constants.getPlayer = function(path){
    var settings = require('./AppSettings');
    return settings.getSync('userAuthentication').player;
};



module.exports = constants;
