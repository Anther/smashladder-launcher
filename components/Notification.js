


class Notification
{
    static showFocusedAlert(title, message, buttons, options){
        const electron = require('electron');
        const remote = electron.remote;
        const mainProcess = remote.require('./index');

        mainProcess.focus();
        return new Promise((resolve, reject)=>{
            var dialog = $('#generic_dialog');
            let footer = dialog.findCache('.modal-footer');
            footer.find('.modal-action').not('.template').remove();
            let template = footer.findCache('.template');
            if(!buttons)
            {
                buttons = [
                    {
                        text:'Ok',
                        classes: 'agree',
                        onClick: ()=>{
                            dialog.modal('close');
                        }
                    }
                ];
            }
            $.each(buttons, function(i,buttonData){
                let button = template.clone().removeClass('template').removeClass('agree');
                button.text(buttonData.text);
                button.on('click', buttonData.onClick);
                if(buttonData.classes)
                {
                    button.addClass(buttonData.classes);
                }
                button.appendTo(footer);
            });
            dialog.find('.title').text(title?title:'');
            console.log(message);
            if(message instanceof String)
            {
                dialog.find('.message').text(message?message:'');
            }
            else
            {
                dialog.find('.message').html(message?message:'');
            }
            let defaultOptions ={
                dismissible: true,
                complete: () => {
                    resolve();
                }
            };
            if(options)
            {
                $.extend(defaultOptions, options)
            }
            dialog.modal(options).modal('open');
        })
    }

    static showFinishedTestNotification(){
        var settings = require('./AppSettings');

        var shouldShow = settings.get('progressSavedOneDolphin').then( value=>{
            if(!value)
            {
                Notification.showFocusedAlert('Ready!', 'Now you can minimize this application and it will ' +
                    'automatically launch dolphin for you on Smashladder.com!');
                settings.set('progressSavedOneDolphin', true);
            }
        });
    }

    static requestAuthenticationPopup(){
        const electron = require('electron');
        const remote = electron.remote;
        const mainProcess = remote.require('./index');

        mainProcess.focus();

        $('#request_authentication').modal({
            dismissible: false,
            complete: function() {

            }
        }).modal('open');
    }
}

module.exports = Notification;