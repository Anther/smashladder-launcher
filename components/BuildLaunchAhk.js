"use strict";

const constants = require('./constants');
const child = require('child_process');

const {app} = require('electron');
const attempt = require('attempt');

const EventEmitter = require('events');
const fileSystem = require('fs');
require ('hazardous');
const path = require ('path');

const $ = require('jquery');

const BuildLaunchSmartDolphin = require('./BuildLaunchSmartDolphin');
const DolphinResponse = require("./DolphinResponse.js");
const DolphinActions = require("./DolphinActions.js");
const shiningStats = require("./ShiningStatsConnection.js");
const DolphinChecker = require("./DolphinChecker.js");
const SendChatInjector = require("./SendChatInjector.js");

const hotkeyLocation = path.join(constants.root,'/external/ahk/NetPlayHotkeySL.exe');

class BuildLaunchAhk extends EventEmitter
{
    constructor(){
        super();
        this.hotkey = null;
        this.sendChatInjector = null;
        this.chatInjectStarter = null;
    }

    launchSendChatInjector(message){
        if(!this.sendChatInjector)
        {
            this.sendChatInjector = new SendChatInjector();
        }
        this.sendChatInjector.postMessage(message);
    }

    async launchHotKey(command, build){
        // console.log('here?!');
        // let interval = setInterval(()=>{
        //     this.sendChatMessage('Rawr a message').catch((error)=>{
        //         console.log('interval catch?');
        //     });
        // }, 2000)

        shiningStats.connect();
        return new Promise((resolve,reject)=>{
            this.killHotkey()
                .then(()=>{
                    const parameters = [];
                    parameters.push('/force');
                    if(typeof command === 'string')
                    {
                        parameters.push(command);
                    }
                    else
                    {
                        for(var i in command)
                        {
                            if(command.hasOwnProperty(i))
                            {
                                parameters.push(command[i]);
                            }
                        }
                    }
                    console.log('Opening Smash Quick Play SL', parameters);
                    this.hotkey = child.spawn(hotkeyLocation, parameters);
                    this.hotkey.on('error', function(err) {
                        console.log('Oh noez, teh errurz: ' + err);
                    });
                    this.hotkey.stdout.on('data', (data) => {
                        if(!data)
                        {
                            console.log('Empty?');
                            return;
                        }
                        var strings = data.toString().split(/\r?\n/);
                        for(var i in strings)
                        {
                            if(!strings.hasOwnProperty(i))
                            {
                                continue;
                            }
                            var stdout = strings[i];
                            if(!stdout)
                            {
                                continue;
                            }
                            // console.log(stdout);
                            stdout = JSON.parse(stdout);

                            var result = DolphinResponse.ahkResponse(stdout);
                            // console.log(result);
                            if(DolphinActions.isCallable(result.action))
                            {
                                DolphinActions.call(result.action, build, result.value)
                            }
                            else
                            {
                            }
                        }
                    });
                    if(!this.hotkey.pid)
                    {
                        throw 'Error loading up SmashQuickPlay';
                    }
                    this.hotkey.on('close', (e)=>{
                        this.hotkey = null;
                    });

                    resolve(true);
                })
        })
    }

    async killHotkey(){
        if(this.hotkey){
            return new Promise((resolve, reject)=>{
                console.log('Killing Hotkey');
                this.hotkey.kill();

                var checkForDeadHotkey = ()=>{
                    if(this.hotkey === null)
                    {
                        setTimeout(()=>{
                        console.log('WAS KILLED');
                            resolve();
                        },500)
                    }
                    else
                    {
                        this.hotkey.kill();
                        setTimeout(()=>{
                            checkForDeadHotkey()
                        }, 250);
                    }
                };
                checkForDeadHotkey();

            });
        }
        else
        {
            return Promise.resolve();
        }
    }

    startGame(){
        console.log('command to start game');
        this.launchHotKey('launch');
    }

    sendChatMessage(message){
        console.log('command to send message');
        return;
        return this.launchSendChatInjector(message);
    }

    launch(build){
        console.trace('at open');
        return BuildLaunchSmartDolphin
            .launch(build, null, true)
            .then(()=>{
                const dolphinProcess = BuildLaunchSmartDolphin.child;
                if(dolphinProcess)
                {
                    dolphinProcess.on('close', ()=>{
                        this.killHotkey();
                    });
                }
                var parameters = ['launch'];
                parameters.push(constants.getPlayer().username);
                parameters.push(build.name);
                return this.launchHotKey(parameters, build)
            })
            .catch((error)=>{
                throw error;
            });
    }

    host(build, gameLaunch){
        console.trace('at host');
        return BuildLaunchSmartDolphin
            .launch(build, null, true)
            .then(()=>{
                if(!build.launchCount)
                {
                    build.launchCount++;
                }
                const dolphinProcess = BuildLaunchSmartDolphin.child;
                if(dolphinProcess)
                {
                    dolphinProcess.on('close', ()=>{
                       this.killHotkey();
                    });
                }
                var parameters = ['host'];
                parameters.push(constants.getPlayer().username);
                if(gameLaunch)
                {
                    if(gameLaunch.gameId)
                    {
                        build.addGameLaunch(gameLaunch.gameId);
                        build.save();
                    }
                    parameters.push(gameLaunch.launch);
                    parameters.push(gameLaunch.game);
                    parameters.push(build.name);
                }
                return this.launchHotKey(parameters, build)
            })
            .catch((error)=>{
                throw error;
            });
    }

    join(build, hostCode){
        return BuildLaunchSmartDolphin
            .launch(build, null, this.closePrevious)
            .then((newChild)=>{
                if(!newChild)
                {
                    throw 'Dolphin already open!';
                }
                return new Promise((resolve, reject)=>{
                    var parameters = ['join'];
                    parameters.push(constants.getPlayer().username);
                    parameters.push(hostCode);
                    parameters.push(build.name);
                    this.launchHotKey(parameters, build)
                        .then(()=>{
                            resolve();
                        })
                        .catch(()=>{
                            reject()
                        });
                });
            })
            .then(()=>{
                const dolphinProcess = BuildLaunchSmartDolphin.child;
                if(dolphinProcess)
                {
                    dolphinProcess.on('close', ()=>{
                        this.killHotkey();
                    });
                }
                var hostCodeElement = build.element.find('.host_code');

            })
            .then(()=>{
                console.log('resolved?');
            })
    }


}

module.exports = new BuildLaunchAhk();