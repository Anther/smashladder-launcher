const winprocess = require('winprocess');
console.log(winprocess);

class LadderMemoryProcessor
{
    constructor(){
        this.dolphin = null;
        this.pid = null;
        this.offset = 0x7FFF0000;
        this.calls = 0;
    }

    static getDolphinPid(){
        var pid = winprocess.getProcessId("Dolphin.exe");
        if(pid < 0)
        {
            return null;
        }
        return pid;
    }

    attach(){
        var pid = LadderMemoryProcessor.getDolphinPid();
        if(!pid)
        {
            throw new Error('no pid');
        }
        this.dolphin = new winprocess.Process(pid);
        this.pid = pid;
        this.dolphin.open();
        return Promise.resolve();
    }

    detach(){
        if(this.dolphin)
        {
            //Calling close crashes the process..
        }
        this.dolphin = null;
    }

    readProcessMemory(address, length){
        this.calls++;
        if(!this.dolphin)
        {
            throw new Error('Dolphin Not Attached!');
        }
        address = address + this.offset;
        var uint8Array = this.dolphin.readMemory(address, length);
        if(!(uint8Array instanceof Uint8Array))
        {
            throw new Error('Error reading memory');
        }
        let buffer = uint8Array.buffer.slice(0);
        return new DataView(buffer);
        uint8Array = null;
        return Promise.resolve(view);
        // let view = uint8Array.buffer;
    }
}

module.exports = LadderMemoryProcessor;