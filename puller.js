
"use strict";
var $ = null;
var jQuery = null;
jQuery = $ = require('./node_modules/jquery/dist/jquery.min.js');

const electron = require('electron');
const ipc = electron.ipcRenderer;
const shell = electron.shell;
const remote = electron.remote;
const mainProcess = remote.require('./index');


const StatPuller = require('./components/StatPuller.js');
const LadderMemoryProcessor = require('./components/LadderMemoryProcessor');
const processMemory = require('processMemory');

const statServer = require("socket.io")(5000);
var statPuller;

// statPuller = new StatPuller(new LadderMemoryProcessor);
statPuller = new StatPuller(processMemory);
statPuller.setServer(statServer);
statPuller.start();

ipc.on('closePuller', function(event){
    console.log('closing');
    if(statServer)
    {
        statServer.close(true);
    }
    LadderMemoryProcessor.detach();
    if(statPuller)
    {
        statPuller.stop();
    }
});