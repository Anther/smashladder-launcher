"use strict";



const electron = require('electron');
const app = electron.app;
const Tray = electron.Tray;
const ipcMain = electron.ipcMain;
const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;
const constants = require('./components/constants');

const Files = require("./components/Files.js");

const SocketManager = require('./components/SocketManager');
var Authentication = require("./components/Authentication.js");
var Settings = require("./components/AppSettings.js");
require("./components/AutoUpdater");
const DolphinChecker = require("./components/DolphinChecker.js");

var info = require('./package.json');
let Build = require('./components/Build.js');

var mainWindow = null;
let tray = null;

const squirrelEvent = require('./components/Squirrel');
var ObjectSort = require("./components/helpers/ObjectSort.js");
var buildsSorted = null;

if (require('electron-squirrel-startup')) return;

// this should be placed at top of main.js to handle setup events quickly
if (squirrelEvent(app)) {
    // squirrel event handled and app will exit in 1000ms, so don't do anything else
    return;
}

const shouldQuit = app.makeSingleInstance((commandLine, workingDirectory) =>{
    // Someone tried to run a second instance, we should focus our window.
    if(mainWindow)
    {
        mainWindow.show();
        mainWindow.restore();
        mainWindow.focus();
    }
});

if (shouldQuit) {
    app.quit();
    return;
}



function initializeTray(){
    var loginSettings = getLoginItemSettings();
    console.log('autoUpdates', !!Settings.getSync('autoUpdates'));
    let quickHost = {
        label: "Host",
        submenu: [

        ]
    };
    let quickJoin = {
        label: "Join",
        submenu: [

        ]
    };
    const LadderGame = require('./components/LadderGame');
    LadderGame.getGames().forEach((game)=>{
        let hostBuilds = [];
        let joinBuilds = [];
        let buildsToUseSorted = ObjectSort.sortByPropertyWithCallback(buildsSorted, function(build){
           return build.getTotalLaunches();
        });
        buildsToUseSorted.reverse().forEach((build)=>{
            if(!build.forLadders)
            {
                return;
            }
            if(!build.forLadders[game.getId()])
            {
                return;
            }
            let name = build.name + '('+build.getTotalLaunches()+')';
            hostBuilds.push({
                label: name,
                click: ()=>{
                    mainWindow.webContents.send('hostGame', game.serialize(), build.dolphin_build_id);
                    updateTray();
                }
            });
            joinBuilds.push({
                label: name,
                click: ()=>{
                    mainWindow.webContents.send('joinGame', game.serialize(), build.dolphin_build_id);
                    updateTray();
                }
            })
        });

        quickHost.submenu.push({
            label: game.getTitle(),
            submenu: hostBuilds,
            // click: ()=>{
            //     mainWindow.webContents.send('hostGame', game.serialize());
            // }
       });
        quickJoin.submenu.push({
            label: game.getTitle(),
            submenu: joinBuilds,
            // click: ()=>{
            //     mainWindow.show();
            //     mainWindow.webContents.send('joinGame', game.serialize());
            // }
       });
    });
    return [
        {
            label: 'Open Smashladder Stats Tracker '+info.version,
            click:  function(){
                mainWindow.show();
            }
        },
        {
            label: null,
            type: 'separator'
        },
        quickHost,
        quickJoin,
        {
            label: 'Random Match',
            enabled: false,
        },
        {
            label: null,
            type: 'separator'
        },
        {
            label: 'Launch At Startup',
            type: 'checkbox',
            checked: loginSettings.openAtLogin,
            click:  function(){
                changeLaunchAtStartup(!getLoginItemSettings().openAtLogin);
                updateTray();
            }
        },
        {
            label: 'Enable Auto Updates',
            type: 'checkbox',
            checked: true,
            enabled: false,
            click:  function(){
                Settings.set('autoUpdates', !Settings.getSync('autoUpdates'));
                updateTray();
            }
        },
        {
            label: null,
            type: 'separator'
        },
        {
            label: 'Minimize To Tray',
            type: 'checkbox',
            checked: Settings.getSync('minimizeToTray'),
            enabled: true,
            click:  function(){
                Settings.set('minimizeToTray', !Settings.getSync('minimizeToTray'));
                updateTray();
            }
        },
        {
            label: null,
            type: 'separator'
        },
        {
            label: 'Quit',
            click:  function(){
                app.isQuiting = true;
                app.quit();
            }
        }
    ]
}

function updateTray()
{
    var items = initializeTray();
    const contextMenu = Menu.buildFromTemplate(initializeTray());
    tray.setContextMenu(contextMenu);
}

app.setAppUserModelId('com.squirrel.smashladderlauncher.SmashladderLauncher');
// Settings.set('startLaunchInitialized', false);
app.on('ready', function () {
    var startupLaunchInitialized = Settings.getSync('startLaunchInitialized');
    var startMinimized = (process.argv || []).indexOf('--hidden') !== -1;

    if(!startupLaunchInitialized)
    {
        changeLaunchAtStartup(true);
        Settings.set('startLaunchInitialized', true);
    }
    // const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(null);

    app.on('window-all-closed', function() {
        app.quit();
    });

    tray = new Tray(constants.root+'/images/android-icon-48x48.png');
    tray.setToolTip('SmashLadder Stats Tracker');
    updateTray();



    tray.on('click', () => {
        mainWindow.isMinimized() ? mainWindow.show() : mainWindow.show()
    });
    tray.on('double-click', function() {
        mainWindow.show();
    });


    var windowOptions = {
        width: 720,
        height: 800,
        resizable: false,
        icon: __dirname + '/images/android-icon-96x96.png'
    };
    if(constants.debugConsole)
    {
        windowOptions.width = 1200;
    }
    if (startMinimized == true){
        windowOptions.show = false;
    }
    mainWindow = new BrowserWindow(windowOptions);
    mainWindow.loadURL('file://' + __dirname + '/index.html');

    mainWindow.on('close', function (event) {
        // if( !app.isQuiting){
        //     event.preventDefault();
        //     mainWindow.hide();
        // }
        // return false;
    });

    ipcMain.on('buildsLoaded', (e, builds)=>{
        buildsSorted = builds;
        for(let i in buildsSorted)
        {
            if(!buildsSorted.hasOwnProperty(i))
            {
                continue;
            }
            buildsSorted[i] = new Build(buildsSorted[i]);
        }
        updateTray();
    });
    ipcMain.on('toggle-dev-tools', ()=>{
        if(constants.debugConsole)
        {
            mainWindow.closeDevTools();
            mainWindow.setSize(720, 800);
        }
        else
        {
            mainWindow.openDevTools();
            mainWindow.setSize(1200, 800);
        }
        constants.debugConsole = !constants.debugConsole;
    });


    mainWindow.on('minimize',function(event){
        if(Settings.getSync('minimizeToTray'))
        {
            event.preventDefault();
            mainWindow.hide();
        }
    });

    if(constants.debugConsole)
    {
        mainWindow.webContents.openDevTools();
    }

    mainWindow.webContents.on("toggle-dev-tools", function (e) {
        alert('we aer here');
        if (e.which === 123) {
            mainWindow.webContents.toggleDevTools();
        }
    });


    mainWindow.on('closed', function() {
        mainWindow = null;
    });

    Files.mainWindow = mainWindow;
    SocketManager.setMainWindow(mainWindow);
    mainWindow.webContents.on('new-window', function(e){
        mainWindow.show();
        e.preventDefault();
    });
    mainWindow.webContents.on('did-finish-load', function() {

        mainWindow.webContents.send('builds-loading');


        SocketManager.on('disableConnection', ()=>{
            mainWindow.webContents.send('requestAuthentication');
        });

        Authentication.load()
            .then(function(authentication){
                console.log('[RETRIEVED AUTHENTICATION]');
                mainWindow.webContents.send('authenticating');
                return authentication.checkAuthentication();
            }).then(function(authentication){
                loginReady(authentication.player);
                return true;
            }).catch(function(error){

                // console.error('[AUTHENTICATION FAILED]', error);
                if(error)
                {
                    if(error.statusCode === 401)
                    {
                        console.log('Authentication is no longer valid :(');
                        Authentication.deleteAuthentication(); //Session was bad
                    }
                    if(error.server_error)
                    {
                    }
                    if(error.no_player)
                    {
                    }
                }
                mainWindow.webContents.send('noPlayerToAuthenticate');
            });

    });

});

var statPuller;
var waitingForDolphin = false;
exports.openStatPuller = ()=>{
    if(statPuller)
    {
        waitingForDolphin = false;
        return;
    }
    waitingForDolphin = true;
    DolphinChecker.waitForDolphinToOpen().then((pid)=>{
        waitingForDolphin = false;
        console.log('OPENING PULLER');
        var directory = __dirname+'/puller.html';
        let windowOptions = {};
        if(constants.showMemoryScannerProcess)
        {
            windowOptions.show = true;
        }
        else
        {
            windowOptions.show = false;
        }
        statPuller = new BrowserWindow(windowOptions);
        statPuller.loadURL('file://' + directory);
        if(constants.showMemoryScannerProcess)
        {
            statPuller.webContents.openDevTools();
        }
        statPuller.on('close', function (event) {
            statPuller = null;
        });


        DolphinChecker.waitForDolphinToClose().then(()=>{
            exports.closeStatPuller();
        })

    });
};

exports.closeStatPuller = ()=>{
    if(statPuller)
    {
        waitingForDolphin = true;
        statPuller.webContents.send('closePuller');
        statPuller.destroy();
        statPuller = null;
    }
};

var disconnectSocketManager = exports.disconnectSocketManager = function(){
    console.log('hwee');
    SocketManager.disconnect();

};

exports.balloonUrl = function(title, content, url){
    tray.displayBalloon({
        title: title,
        content: content,
    });
    tray.on('balloon-show', ()=>{

    });
    tray.removeListener('balloon-show',()=>{

    })
};

exports.focus = function(){
    if(mainWindow)
    {
        mainWindow.focus();
    }
};
exports.minimize = function(){
    if(mainWindow)
    {
        mainWindow.minimize();
    }
};

var loginReady = exports.loginReady = function(player){
    Authentication.load()
        .then(function(authentication) {
            console.log('what do we have here', authentication);
            SocketManager.setAuthentication(authentication);
            SocketManager.connect(authentication);
            mainWindow.webContents.send('loginReady', authentication.player);
        }).catch(error=>{
            throw error;
        });
};
var changeLaunchAtStartup = function(value){
    value = !!value;
    console.log('changing startup options', value);

    var settings = getLoginItemSettingsDefaults();
    settings.openAtLogin = value;
    settings.openAsHidden = true;

    app.setLoginItemSettings(settings);

    console.log(getLoginItemSettings());
};

function getLoginItemSettingsDefaults(){
    const path = require('path');
    const appFolder = path.dirname(process.execPath);
    const updateExe = path.resolve(appFolder, '..', 'Update.exe');
    const exeName = path.basename(process.execPath);
    return {
        path: updateExe,
        args: [
            '--processStart', `"${exeName}"`,
            '--process-start-args', `"--hidden"`
        ]
    }
}
function getLoginItemSettings(){
    var defaults = getLoginItemSettingsDefaults();
    return app.getLoginItemSettings(defaults);
}
exports.app = app;
exports.selectDolphinLocation = function(){
    return Files.selectDolphinLocation(mainWindow);
};