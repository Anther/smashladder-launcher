"use strict";

var $ = null;
var jQuery = null;
jQuery = $ = require('./node_modules/jquery/dist/jquery.min.js');

const fetch = require('node-fetch');

const electron = require('electron');
const ipc = electron.ipcRenderer;
const shell = electron.shell;
const remote = electron.remote;
const mainProcess = remote.require('./index');

const constants = require("./components/constants");
const SmashladderApi = require("./components/SmashladderApi");

const Authentication = require("./components/Authentication.js");
const clipboard = electron.clipboard;

const Builds = require("./components/Builds.js");
const Notification= require("./components/Notification.js");


var ViewManager = require("./components/ViewManager");
const BuildLaunch = require("./components/BuildLaunchSmartDolphin");
const settings = require("./components/AppSettings");
ViewManager = new ViewManager($('#container'), $('#content'));

let currentPlayer = null;


document.addEventListener("keydown", function (e) {
    if (e.which === 123){
        ipc.send('toggle-dev-tools');
    }
});


ipc.on('noPlayerToAuthenticate', function(event){
    ViewManager.changeView('login');
});
ipc.on('socketConnected', function(event){
    $('#logged_in').addClass('socket_connected').removeClass('socket_connecting');
});
ipc.on('socketDisconnected', function(event){
    $('#logged_in').removeClass('socket_connected').addClass('socket_connecting');
});
ipc.on('builds-loading', function(event){
   ViewManager.changeView('loading');
});
ipc.on('startNetplay', function(event, message){
    let forceClose = message.data && message.data.force_close;
    Builds.launchDolphinBuildById(message.data.dolphin_version.id, message, forceClose)
        .then((build) => {
            var successfulLaunches = settings.set('build');
        }).catch((error) =>{
            console.error('Issue launching build ' , error, message);
        });

});
ipc.on('hostNetplay', function(event, message){
    if(message.data.dolphin_version === null)
    {
        Notification.showFocusedAlert('Launch Error', 'No common dolphin build found to launch');
        return;
    }
    try{
        Builds.launchDolphinBuildById(message.data.dolphin_version.id, message, true);
    }
    catch(error){
        if(error && error.error)
        {
            Notification.showFocusedAlert("Error!", error.error);
        }
    }
});

ipc.on('sendChatMessage', function(event, message){
    let messageComponent = message.data.message;
    let messageSend = null;
    if(messageComponent.sender.id == currentPlayer.id)
    {
        messageSend = messageComponent.message;
    }
    else
    {
        messageSend = '['+messageComponent.sender.username +']: '+ messageComponent.message;
    }
    Builds.sendChatMessage(message.data.dolphin_version.id, messageSend);
});

ipc.on('startGame', function(event, message){
    Builds.startGame(message.data.dolphin_version.id);
});

ipc.on('quitDolphin', function(event){
    BuildLaunch.killChild()
        .then((response)=> {
        })
        .catch((error)=> {
            console.log('[QUIT ERROR?]');
        });
});

ipc.on('loginReady', function(event, player){
    currentPlayer = player;
    $('#logged_in').addClass('active')
        .find('.username').text(player.username);
    ViewManager.changeView('dolphins');
});

ipc.on('requestAuthentication', function(event, buildName){

    Notification.requestAuthenticationPopup();
});

$('#request_authentication').on('click','.agree', function(e){
   mainProcess.loginReady(); 
});

var logoutButton = $('#logout_button');
logoutButton.on('click', function(e){

    if(logoutButton.hasClass('logging_out'))
    {
        return;
    }

    var finished = function(){
        $('#logged_in').removeClass('active');
        logoutButton.removeClass('logging_out');
        ViewManager.changeView('login');
        Authentication.deleteAuthentication();
        mainProcess.disconnectSocketManager();
    };
    logoutButton.addClass('logging_out');

    Authentication.load()
        .then(function(authentication){
            if(authentication)
            {
                return SmashladderApi.apiv1Post(constants.apiv1Endpoints.LOGOUT);
            }
            else
            {
                throw 'No authentication found';
            }
    }).then(function(response){
        finished();
    }).catch(function(error){
        finished();
    });

});

$(document).on('click', 'a[href^="http"]', function (event) {
    event.preventDefault();
    shell.openExternal(this.href);
});

