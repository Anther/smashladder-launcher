var packager = require('electron-packager');

options = {
    dir: '.',
    overwrite: true,
    win32metadata: {
        CompanyName:'SmashLadder',
        ProductName:"SmashladderLauncher",
    },


    name: 'SmashladderLauncher',
    asar: {
        unpackDir: "external"
    },
    platform: 'win32',
    // arch: 'x64',
    ignore: [
        "README.md",
        '.gitignore',
        '/cert($|/)',
        '/release-builds($|/)',
        '/.idea($|/)',
        '/screenshots($|/)',
        '/release-private($|/)',
        '/dolphins($|/)',
        '/test($|/)',
    ],
    icon: './images/icon.ico',
    prune: true,
    out: 'release-builds'
};
console.log('Packaging...');
var interval = setInterval(function(){
    process.stdout.write('.');
}, 1000);
packager(options, function done_callback (err, appPaths) {
    clearInterval(interval);
    if(err)
    {
        console.log('error');
        console.error(err);
    }
    else
    {
        console.log('Donesies');
    }
    console.log(appPaths);
});